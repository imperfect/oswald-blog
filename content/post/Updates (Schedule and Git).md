+++
draft = false
author = "Ozzy Meadows"
title = "Updates (Schedule and Git)"
date = 2020-08-28
+++
I had intentions of posting about a week ago, but stuff/things/life happened, and I didn't; so, I've decided to only post on Sundays from now on.
<!--more-->

By only posting on Sunday, I'll give myself a deadline, and if I happen to finish it a post early, I won't feel compelled to post it right away; instead, I can review it some days later. I know that it's not like I need a deadline, but I'm disappointed in myself that I said I would work on my post and didn't put my full efforts into it.

Also, I will be adding a git repository for the blog soon to mark all changes and additions. This repository will be linked to on any posts that are altered after the initial posting. That should be completed by Monday (after I hopefully finish my post for Sunday) along with some needed corrections to previous posts.

