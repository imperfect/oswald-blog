+++
draft = false
author = "Ozzy Meadows"
title = "Why a Static Website can Be Great"
date = 2020-08-17
description = "Change this"
categories = [
    "Software",	
]
+++
A static website is one that serves fixed content, making it (comparatively) simple to a dynamic one. The result is that static websites are significantly faster and have a significant drawback of not supporting user logins, comments, etc. But for a lot of sites, that isn't a problem.
<!--more-->

That said, static and dynamic websites are not mutually exclusive in-fact, you can easily have, say, the comments on your static website handled by Disqus, which uses an iframe on your website to provide comments. An iframe is like a frame or window on your webpage that loads from another site. Some more examples of this are Youtube videos on a site that isn't Youtube or Facebook comments on a site that isn't Facebook.

One other advantage of static websites is that they can run hosted on object storage, which has ups and downs. One of the upsides is that you don't have to worry about managing a web server, just the object storage (basically nothing once setup). The main downside is that HTTPS becomes troublesome. There may be some solutions to this, but for a site like this one,  HTTPS is only useful for preventing pharming attacks, and I don't see that as much of a threat, so none of the solutions seem to be worth the trouble.

Before I continue, I have to mention Bryan Lunduke's post covering Hugo ["The Beauty of a Static Website with Hugo"](https://lunduke.com/posts/2020-03-03/) which introduced me to Hugo and is worth a read if you're interested in Hugo or static websites.

# Hugo
Quote from [Hugo's](https://gohugo.io/) homepage:

> Hugo is one of the most popular open-source static site generators. With its amazing speed and flexibility, Hugo makes building websites fun again.

There are a plethora of pre-made themes/templates available on [Hugo's site](https://themes.gohugo.io/), or you can make your own with CSS, HTML, and JS. I decided to use a pre-made theme because I'm not yet ready to put the time in to making my own good looking and functional theme.

I had some difficulties with using Hugo at the start, but most were solved with a little bit of trial, error, and reading; however, I could never figure out how to get archetypes to function. I'm quite sure I followed the documentation correctly, so I assume I'm dealing with some bug; so, I'll have to do some more troubleshooting later,  but for now, I'll simply use copypaste. I should've spent an hour or so to get familiar with the docs before following them; I'll try and do this in the future so I can hopefully have fewer issues, and if I do have an issue, I can decide if its a bug or not in less time.

In the future, I would like to investigate some of Hugo's [frontends/GUIs](https://gohugo.io/tools/frontends/)  to see if they are easy enough to use for someone without much computer experience. I would love to suggest something in the steed of Squarespace or Wix because I simply don't like their products, but they're the only easy to use options I know of.

As far as creating pages with Hugo goes, it works quite well using Markdown or a document writer that supports HTML export. I use Standard Notes Markdown Pro and Plus Editor. Normal Markdown lacks some crucial features (like text alignment), which make creating some styles of content difficult, though I just put HTML into the Markdown or use the Plus Editor when needed. I imagine a GUI could make this whole process very easy, but I'll see when I get around to checking those out.

# Alternatives to Hugo
As I was making this post, I came across a [video (watch for context)](https://www.youtube.com/watch?v=N_ttw2Dihn8) by [Wolfgang](https://www.youtube.com/channel/UCsnGwSIHyoYN0kiINAGUKxg), which has some valid points regarding most static website generators. I will be switching to something akin to the mentioned in the future, but because it lacks some features I would like, I will need to create some of this functionality, and that may take some time.
