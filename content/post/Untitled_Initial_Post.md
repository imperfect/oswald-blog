+++
draft = false
author = "Ozzy Meadows"
title = "Intentions for the Blog"
date = 2020-08-15T20:48:33-05:00
description = "Initial post regarding my thoughts on the blog."
categories = [
    "Nontechnical",	
]
+++
I'm making this blog for fun and to progress my writing skills along with whatever other topics I'm posting about. 
<!--more-->

Initially, I was going to post some of my thoughts on movies I enjoyed, but I just felt I didn't quite know what I was doing. So instead, I'm going to focus on posting about InfoSec, software, and puzzles for now, and later I'll post various other types of content once I'm more familiar with the format.

I do expect to make some mistakes, so if you do notice any, I would appreciate it if you would send me an email (found at [about](/about)).

## Future Posts
I would like to post fortnightly, if not weekly. My first post regards static websites and should be up shortly after the blog is up. After that, I have a post covering proper passwords mainly for my friends & family, which should be up within a few days. 

Beyond that, I hope to in the next couple of weeks, to do the required research to be fully informed regarding the Linux program `sudo` and write a post regarding its security. I plan on this being the first of many posts on the securities of GNU/Linux. TLDR: it's not secure (at least for the desktop user).

I guess I've been putting off releasing this blog for a while now. It's not perfect, but here you go. :)

## A Note On the Domain Name
I thought about using a more "professional" domain name (like "ozzymeadows.com"), yet I couldn't do it. I already had this domain name for my Nextcloud instance, and I enjoy it. And that's okay; I mean this blog is mainly for me anyway.
