+++
draft = false
author = "Ozzy Meadows"
title = "Why You Should Use a Password Manager"
date = 2020-08-30
description = "Initial post regarding my thoughts on the blog."
categories = [
    "Nontechnical",
]	
+++
Companies are routinely breached, often resulting in customer information being leaked, including credit card information, passwords, emails, private messages, etc. To worsen the issue, users frequently reuse passwords on multiple websites. So, when one website gets breached, the attackers/hackers can use the credentials they got from that breach to attempt to login on other websites, often succeeding.<!--more-->

Remembering dozens of unique passwords is a chore, and the result is that these passwords lose their complexity; for instance, someone may use the passwords pancakes29 and pancakes30, which (as you can tell) is hardly a difference whatsoever. In general, it's a good idea to shift your mindset towards passphrases instead of passwords. I encourage you to watch [Edward Snowden on Passwords: Last Week Tonight with John Oliver (HBO)](https://www.youtube.com/watch?v=yzGzB-yYKcc), which talks about the use of passphrases instead of passwords.

# Why a Password Manager
A password manager removes the need to remember the majority of your passwords. This enables you to use more complex passwords and increases your resistance to phishing/pharming attacks.

Convenience is another factor to consider. Setting up the password manager may take some effort, but once it's done, your set. Auto-fill is an incredibly convenient feature; all you need to do is remember the password to your manager, type it in every so often, and claim victory with auto-fill.

The biggest argument against password managers is that it keeps all your passwords in one place, and I agree, this is a valid point. However, keep in mind that, normally passwords are already kept in one place, your memory. Now you may think your memory is a safe place, but phishing attacks and social engineering are far more effective than most people think. Password managers significantly mitigate this risk. Phishing and social engineering become much less effective when you only remember one password, and you know to never share it.

# Choosing a Password Manager

Password manager types include paper, local, cloud, and generators. Cloud makes the most sense for most people because of the features, and you don't have to worry about backing up any files. Generator types have yet to mature, so it's hard for me to suggest them. If you're interested in the reduced attack surface that local and generator types offer, take a look at the [privacytools.io page on password managers](https://www.privacytools.io/software/passwords/).
 
Keep in mind that the feature in your browser that asks save your passwords is not a proper password manager and should be used with caution. They also lack a plethora of features available within proper managers. This may change in the future, but at the moment, I don't suggest you use them for anything important. Although, Firefox Lockwise seems to have potential.

If you decide against using a password manager or opt for a paper/local/generator type, I highly recommend that you put your email address used to create your accounts into the  [have I been pwned?](https://haveibeenpwned.com/) website. This will notify you if your credentials have been known to be breached.

## cloud-based
A cloud-based password manager allows you to access your passwords from anywhere with (hopefully) secure credentials. Lastpass, Dashlane, and 1Password are some well-known ones. I highly recommend Bitwarden for the maximum security and privacy available with a cloud-based manager, but not for features. Bitwarden does check for breaches passwords but does not support automatic password changes, which just I've never tried, but sounds magical. The others do support automatic password changing, and I'm sure some other useful features.

## Paper
Using a piece of paper to store passwords can be effective; however, this isn't ideal for mostly obvious reasons. The main one being, you have to type in the passwords, which leads to reduced password complexity. Because of this, using paper is best used to backup passphrases you must remember. You should avoid labeling any passphrases you note down if you can remember what it's for.

# How to properly use a Password Manager
1. I can not stress enough; to not ever share your password to your manager; not even to the company that runs it. I also encourage you to enable 2FA on your cloud-based manager.

3. Your password manager shouldn't be unlocked all the time; your login should at least expire every 24 hours. If this sounds cumbersome, fingerprint authentication can be a good, but less secure, compromise.

4. Let your manager auto-fill your login credentials. If your manager doesn't auto-fill and it normally does work for that app/site, you may have encountered a phishing attempt. If this happens, you should verify that it is the true login page.

5. Use your manager to automatically generate passwords instead of creating them yourself. The longer and more character types the password has, the better (16 characters long should be the minimum length used). Keep in mind that some websites won't accept the password if you make it too complex or too long.

6.  When a website asks you to create security questions (the "What was the name of your first pet" and so on), I highly recommend you generate a password with your manager, use that as the answer, and then copy that answer into the notes field of your entry along with the question.

7. There are some kinds of passwords you don't want to put into your manager. Like, the passwords to your devices, FED, or the manager itself. Not that I'm stopping you, just that often these kinds of passwords don't have a good reason to be in your manager, these are the kind of passwords you should remember.

# 2FA
Two-factor Authentication is very important if you want the maximum security for an account. 2FA is simply a combination of different authentication methods, which are something you know, something you have, and something you are (biometrics). For a deeper explanation of 2FA, I highly recommend [Tom's Scott's Video: Why You Should Turn On Two Factor Authentication](https://www.youtube.com/watch?v=hGRii5f_uSc). I suggest you set up 2FA on any accounts you deem important. Do note that 2FA via SMS is much less secure and should only be used if nothing else is available.

One tactic that attackers/hackers use is to enable 2FA when they get access to an account, resulting in it being much harder for you to get back access to your account. To prevent this, enable 2FA before they do.

# Update History

| Date                | Link                                                                                                                                                                                                                                                                         |
| ------------  | ---------------------------------------------------------------------------------------------------------------------------------------------  |
| 2020-09-01 | [Gitlab history](https://gitlab.com/imperfect/oswald-blog/-/commits/master/content/post/Why_You_Should_se_a_Password_Manager) |
